# k8s



## What is this?

These are the Kubernetes resources I am running at my various Labs. 

- nas - TrueNAS scale based lab at home

## Bootstrap/Upgrade

```
export GITLAB_TOKEN=<token>
export CLUSTER_NAME=nas

# connect to gitlab via api, create an ssh key for long term cluster<->gitlab authentication
# requires active kubectl to the cluster
flux bootstrap gitlab --owner=Gregy --repository=k8s --private=false --personal --path clusters/"$CLUSTER_NAME"
```

## Problems and solutions

### Connect to TrueNAS k3s via kubectl

1. SSH to Truenas and copy the kubeconfig file from /etc/rancher/k3s/k3s.yaml
2. Create ssh tunnel to reach the k8s api `ssh admin@nas.gregy.cz -L 6443:localhost:6443`
